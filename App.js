/*

App.js
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  ImageBackground,
  Pressable,
  TouchableOpacity,
  Image,
} from 'react-native';
import thunk from 'redux-thunk';
import React, {
  useState,
  useEffect,
} from 'react';
import {
  Provider,
  connect,
} from 'react-redux';
import SmoothPicker from "react-native-smooth-picker";
import {
  fetchDiceOwned,
  loadingDiceOwned,
  applyFilter,
  enableBuildingArmy,
} from './actions';
import { applyMiddleware, createStore } from 'redux';
import rootReducer from './reducers';
import DiceList from './components/DiceList';
import ImportButton from './components/ImportButton';
import ExportButton from './components/ExportButton';
import DieAmountModal from './components/DieAmountModal';
import ArmyBuilder from './components/ArmyBuilder';
import ArmyBuilderDiePicker from './components/ArmyBuilderDiePicker'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  menu
} from './menu';
const store = createStore(rootReducer, applyMiddleware(thunk))

const HomeScreen = (props, {navigation}) => {
  const [isSpeciesPickerOpen, setSpeciesPickerOpen] = useState(false);
  const [isEditionPickerOpen, setEditionPickerOpen] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);
  const backgroundImage = require('./background.jpeg')
  useEffect(() => {
    if( !isLoaded) {
      props.loadingDiceOwned();
      props.dispatch(fetchDiceOwned());
      
      props.applyFilter('Amazon', '-');
      setIsLoaded(true);
    }
  }, [isLoaded])

  const [selectedEdition, setSelectedEdition] = useState(props.filters.editionFilter);

  useEffect( () => {
      setSelectedEdition(props.filters.editionFilter);
  }, [props.filters, isEditionPickerOpen])

  const [selectedSpecies, setSelectedSpecies] = useState(props.filters.speciesFilter);

  useEffect( () => {
      setSelectedSpecies(props.filters.speciesFilter);
  }, [props.filters.speciesFilter, isSpeciesPickerOpen]);

  function onSpeciesApply(item) {
    setSelectedSpecies(item);
    props.applyFilter(item, '-');
    setSpeciesPickerOpen(false);
    editions = menu[item];
  }

  let species = Object.keys(menu).sort();
  let editions = menu[props.filters.speciesFilter];

  function onEditionApply(item) {
    setSelectedEdition(item);
    props.applyFilter(props.filters.speciesFilter, item);
    setEditionPickerOpen(false);
  }

  function openSpeciesSelector() {
    setSpeciesPickerOpen(true);
  }

  function openEditionSelector() {
    setEditionPickerOpen(true);
  }

  return (
    <SafeAreaView>
    {
        props.isLoading ?
          <View style={styles.activityIndicatorContainerSuper}>
           <ImageBackground source={backgroundImage} style={styles.image}>
            <View style={[styles.activityIndicatorContainer, styles.horizontal]}>
              <ActivityIndicator style={{width: '100%',}} size="large" color="darkred"/>
            </View>
          </ImageBackground>
          </View>
        :
        isSpeciesPickerOpen ?
          <View style={styles.activityIndicatorContainerSuper}>
            <ImageBackground source={backgroundImage} style={styles.image}>
              <View style={[styles.activityIndicatorContainer, styles.horizontal]}>
                <View style={{width: '100%',}}>
                  <SmoothPicker
                      scrollAnimation
                      data={species}
                      initialScrollIndex={21}
                      selectOnPress={true}
                      renderItem={({item, index}) => (
                        <TouchableOpacity style={{width: '100%'}} onPress={() => onSpeciesApply(item)}>
                          <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 18, borderRadius: 5, padding: 15}}>{item}</Text>
                        </TouchableOpacity>
                      )}
                  />
                </View>
              </View>
            </ImageBackground>
          </View>
        :
        isEditionPickerOpen ?
          <View style={styles.activityIndicatorContainerSuper}>
            <ImageBackground source={backgroundImage} style={styles.image}>
              <View style={[styles.activityIndicatorContainer, styles.horizontal]}>
                <View style={{width: '100%',}}>
                  <SmoothPicker
                      initialScrollToIndex={22}
                      scrollAnimation
                      data={editions}
                      selectOnPress={true}
                      renderItem={({item, index}) => (
                        <TouchableOpacity style={{width: '100%'}} onPress={() => onEditionApply(item)}>
                          <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 18, borderRadius: 5, padding: 15}}>{item}</Text>
                        </TouchableOpacity>
                      )}
                    />
                  </View>
                </View>
              </ImageBackground>
          </View>
      :
    <View style={styles.containerContainer}>
      <ImageBackground source={backgroundImage} style={styles.image}>
        <DieAmountModal/>
        <View style={styles.horizontal}>
          <ImportButton/>
          <ExportButton/>
        </View>
        <View style={styles.container}>
          <View style={{padding: 5}}>
            <Pressable onPress={openSpeciesSelector}>
              <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 18, borderRadius: 5, padding: 15, backgroundColor: 'black', color: 'white'}}>{props.filters.speciesFilter}</Text>
            </Pressable>
          </View>
          <View style={{padding: 5}}>
            <Pressable onPress={openEditionSelector}>
              <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 18, borderRadius: 5, padding: 15, backgroundColor: 'black', color: 'white'}}>{props.filters.editionFilter}</Text>
            </Pressable>
          </View>
          <DiceList navigation={navigation}/>
        </View>
      </ImageBackground>
    </View>
    }
    </SafeAreaView>
  );
};

const HomeStack = createNativeStackNavigator();

const HomeStackScreen = () => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="CollectionHome" component={AppHomeScreen} options={{ headerShown: false }}/>
    </HomeStack.Navigator>
  );
};

const ArmyBuilderDiePickerScreen = ({navigation}) => {
  return (
    <ArmyBuilderDiePicker navigation={navigation}/>
  );
};

const ArmyBuilderScreen = ({navigation}) => {
  const backgroundImage = require('./background.jpeg')

  return (
    <ImageBackground source={backgroundImage} style={styles.image}>
      <ArmyBuilder navigation={navigation}/>
    </ImageBackground>
  );
};

const ArmyBuilderStack = createNativeStackNavigator();

const ArmyBuilderStackScreen = () => {
  return (
    <ArmyBuilderStack.Navigator>
      <ArmyBuilderStack.Screen name="ArmyBuilder" component={ArmyBuilderScreen} options={{ headerShown: false }}/>
      <ArmyBuilderStack.Screen name="DiePicker" component={ArmyBuilderDiePickerScreen} options={{ headerShown: false }}/>
    </ArmyBuilderStack.Navigator>
  );
};

const Tab = createBottomTabNavigator();

const Root = () => {
  const style = {width: 20, height: 20,};
  return (
    <NavigationContainer>
      <Tab.Navigator screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            if (route.name === 'Collection Manipulation') {
              return (
                <Image source={require('./images/TE_Tower-1.png')} style={style}/>
              );
            } else if (route.name === 'Army Builder') {
              return (
                <Image source={require('./images/EL_Dragonslayer_Counter-1.png')} style={style}/>
              );
            } else if (route.name === 'Collection') {
              return (
                <Image source={require('./images/Tm_Village-1.png')} style={style}/>
              );
            }
          }
        })}>
        <Tab.Screen name="Collection" component={HomeStackScreen} />
        <Tab.Screen name="Army Builder" component={ArmyBuilderStackScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  )
}

const App = (props) => {
  return (
    <Provider store={store}>
      <Root />
    </Provider>
  );
};

const styles = StyleSheet.create({
  activityIndicatorContainer: {
    flex: 1,
  },
  activityIndicatorContainerSuper: {
    alignSelf: 'center', justifyContent: 'center', alignItems: 'stretch',
  },
  containerContainer: {
    alignSelf: 'flex-start',
  },
  image: {
    flex: 1,
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  image2: {
    flex: 1,
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  container: {
    justifyContent: 'space-around',
    paddingLeft: 16,
    paddingRight: 16,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  toolbar: {
    backgroundColor: 'black',
  },
  toolbarText: {
    fontSize: 24,
    color: 'white',
    padding: 5,
  },
});

const mapStateToProps = (state, props) => {
  const {
    isLoading,
    filters,
    isArmyBuilderDiceSelectorMode,
    isArmyBuilderActive,
  } = state;
  return {
      isLoading,
      filters,
      isArmyBuilderDiceSelectorMode,
      isArmyBuilderActive,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    enableBuildingArmy: (isBuildingArmy) => dispatch(enableBuildingArmy(isBuildingArmy)),
    applyFilter: (speciesFilter, editionFilter) => dispatch(applyFilter(speciesFilter, editionFilter)),
    loadingDiceOwned: () => dispatch(loadingDiceOwned()),
    dispatch,
  };
};

const AppHomeScreen = connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
export default App