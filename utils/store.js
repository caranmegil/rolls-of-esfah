/*

reducers/store.js
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import SQLite from "react-native-sqlite-2";

const db = SQLite.openDatabase("diceOwned.db", "1.0", "", 1);
db.transaction( txn => {
    txn.executeSql(
        "CREATE TABLE IF NOT EXISTS DiceOwned(dieOwned_id INTEGER PRIMARY KEY NOT NULL, diekey VARCHAR(255), amount INTEGER)",
        []
    );
});

export const getStoredDiceOwned = async () => {
    return await new Promise( (resolve, reject) => {
        db.transaction( txn => {
            txn.executeSql("SELECT diekey, amount FROM DiceOwned;", [], (tx, res) => {
                let diceOwned = {}
                for (let i = 0; i < res.rows.length; ++i) {
                    diceOwned[res.rows.item(i).diekey] = res.rows.item(i).amount
                }

                resolve(diceOwned)
            }, (txn, err) => console.log(err));
        });
    });
};

export const clearDatabase = async () => {
    return await new Promise((resolve, reject) => {
        db.transaction(txn => {
            txn.executeSql('DELETE FROM DiceOwned', [], (tx, res) => {
                resolve();
            })
        })
    });
}

export const replaceDatabase = async (myDiceOwned) => {
    return await new Promise( (resolve, reject) => {
        db.transaction( txn => {
            txn.executeSql('DELETE FROM DiceOwned;');
            Object.keys(myDiceOwned).forEach( key => {
                const value = myDiceOwned[key]
                txn.executeSql('INSERT INTO DiceOwned(diekey, amount) VALUES(:diekey, :amount);', [key, value], (tx, res) => {
                }, (txn, err) => console.log(err));
            });
            txn.executeSql('COMMIT;')
            resolve()
        });
    });
}


export const upsertIntoDatabase = async (key, value, myDiceOwned) => {
    return await new Promise( (resolve, reject) => {
        db.transaction( txn => {
            if (myDiceOwned[key] !== undefined) {
                txn.executeSql('UPDATE DiceOwned SET amount = :amount WHERE diekey = :diekey;', [value, key], (tx, res) => {
                    txn.executeSql('COMMIT;')
                    resolve()
                }, (txn, err) => console.log(err));
            } else {
                txn.executeSql('INSERT INTO DiceOwned(diekey, amount) VALUES(:diekey, :amount);', [key, value], (tx, res) => {
                    txn.executeSql('COMMIT;')
                    resolve()
                }, (txn, err) => console.log(err));
            }
        });
    });
}
