/*

reducers/index.js - the reducers
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { combineReducers } from "redux";
import { genKey, genKey2 } from '../utils';
import {
    UPDATE_AMOUNT,
    FILTER_DICE,
    IMPORT_ROLL,
    FETCH_DICE_OWNED,
    LOADING_DICE_OWNED,
    EDIT_DICE_OWNED,
    ARMY_BUILDER_DICE_SELECT,
    BUILDING_ARMY,
    ADD_DICE_TO_ARMY,
    IMPORT_ARMY,
} from '../actions/types';
import {
    data
} from './data';

const filters = (state = {speciesFilter: 'Amazon', editionFilter: '-'}, action) => {
    switch (action.type) {
        case FILTER_DICE:
            return {
                speciesFilter: action.speciesFilter,
                editionFilter: action.editionFilter,
            };
        default:
            return state;
    }
} 

const dice = (state = data, action) => {
    switch (action.type) {
        case FILTER_DICE:
            return data.filter( die => {
                return die.species.toLocaleUpperCase() === action.speciesFilter.toLocaleUpperCase() &&
                        die.edition.toLocaleUpperCase() === action.editionFilter.toLocaleUpperCase();
            });
        default:
            return state;
    }
};

const diceOwned = (state = {}, action) => {
    switch (action.type) {
        case IMPORT_ROLL:
            return {...action.roll}
        case UPDATE_AMOUNT:
            let newDiceOwned = {...state}
            newDiceOwned[genKey2(action.species, action.name, action.edition)] = action.amount
            return newDiceOwned
        case FETCH_DICE_OWNED:
            return {...action.diceOwned};
        default:
            return {...state};
    }
};

const isLoading = (state = false, action) => {
    switch (action.type) {
        case LOADING_DICE_OWNED:
            return action.isLoading;
        default:
            return state;
    }
};

const getDiceEditingState = () => {
    return {
        currentSpecies: '',
        currentName: '',
        currentEdition: '',
        currentAmount: 0,
        isEditingDice: false,
    };
};

const diceEditingState = (state = getDiceEditingState(), action) => {
    switch(action.type) {
        case EDIT_DICE_OWNED:
            return {...action};
        default:
            return state;
    }
};

const isArmyBuilderDiceSelectorMode = (state = false, action) => {
    switch(action.type) {
        case ARMY_BUILDER_DICE_SELECT:
            return action.modeOn;
        default:
            return state;
    }
}

const isArmyBuilderActive = (state = false, action) => {
    switch(action.type) {
        case BUILDING_ARMY:
            return action.isBuildingArmy;
        default:
            return state;
    };
};

const dieToAdd = (state = null, action) => {
    switch(action.type) {
        case ADD_DICE_TO_ARMY:
            return action.dieToAdd;
        case BUILDING_ARMY:
            return null;
        default:
            return state;
    };
};

const importedArmy = (state = null, action) => {
    switch(action.type) {
        case IMPORT_ARMY:
            return action.army;
        default:
            return state;
    }
}

export default combineReducers(
    {
        filters,
        dice,
        diceOwned,
        isLoading,
        diceEditingState,
        isArmyBuilderDiceSelectorMode,
        isArmyBuilderActive,
        dieToAdd,
        importedArmy,
    }
);
  