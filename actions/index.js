/*

actions/index.js - the actions exported
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import {
    FILTER_DICE,
    IMPORT_ROLL,
    UPDATE_AMOUNT,
    FETCH_DICE_OWNED,
    LOADING_DICE_OWNED,
    EDIT_DICE_OWNED,
    ARMY_BUILDER_DICE_SELECT,
    BUILDING_ARMY,
    ADD_DICE_TO_ARMY,
    REMOVE_FIRST_ARMY_DIE,
    IMPORT_ARMY,
} from './types';
import {
    genKey2
} from '../utils';
import {
    getStoredDiceOwned,
    upsertIntoDatabase,
    replaceDatabase,
  } from '../utils/store';
  
export const applyFilter = (speciesFilter='Amazon', editionFilter='-') => {
    return {
        type: FILTER_DICE,
        speciesFilter,
        editionFilter,
    };
};

export const updateAmount = (species, name, edition, amount) => {
    return {
        type: UPDATE_AMOUNT,
        species,
        name,
        edition,
        amount,
    };
};

export const importRoll = (roll) => {
    return {
        type: IMPORT_ROLL,
        roll,
    };
};

export const importArmy = (army) => {
    return {
        type: IMPORT_ARMY,
        army,
    };
};

export const getDiceOwned = (diceOwned) => {
    return {
        type: FETCH_DICE_OWNED,
        diceOwned,
    };
};

export const loadingDiceOwned = (isLoading = true) => {
    return {
        type: LOADING_DICE_OWNED,
        isLoading,
    };
};

export const editDiceAmount = (currentSpecies, currentName, currentEdition, currentAmount, isEditingDice) => {
    return {
        type: EDIT_DICE_OWNED,
        currentSpecies,
        currentName,
        currentEdition,
        currentAmount,
        isEditingDice,
    };
}

export const enableArmyBuilderDiceSelector = (modeOn) => {
    return {
        type: ARMY_BUILDER_DICE_SELECT,
        modeOn,
    };
}

export const enableBuildingArmy = (isBuildingArmy) => {
    return {
        type: BUILDING_ARMY,
        isBuildingArmy,
    };
};

export const addDieToArmy = (dieToAdd) => {
    return {
        type: ADD_DICE_TO_ARMY,
        dieToAdd,
    };
};

export const removeFirstArmyDie = (die) => {
    return {
        type: REMOVE_FIRST_ARMY_DIE,
        die,
    }
}

export const fetchDiceOwned = () => {
    return function(dispatch) {
        dispatch(loadingDiceOwned(true));
        return getStoredDiceOwned().then(
            diceOwned => {
                dispatch(getDiceOwned(JSON.parse(JSON.stringify(diceOwned).replace(/Undeads/g, 'Undead'))))
                dispatch(loadingDiceOwned(false))
            },
            error => console.log(error)
        );
    };
};

export const saveDatabase = (newDiceOwned) => {
    return function(dispatch, getState) {
        dispatch(loadingDiceOwned(true))
        return replaceDatabase(newDiceOwned).then( () => {
            dispatch(getDiceOwned(newDiceOwned));
            return dispatch(loadingDiceOwned(false));
        });
    };
};

export const upsertDiceOwned = (species, name, edition, value) => {
    return function(dispatch, getState) {
        return upsertIntoDatabase(genKey2(species, name, edition), value, getState().diceOwned)
        .then(() => dispatch(fetchDiceOwned()), (err) => console.log(err))
    };
};