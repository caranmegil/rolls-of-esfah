/*

actions/types.js - the action types
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

export const FILTER_DICE = 'FILTER_DICE';
export const UPDATE_AMOUNT = 'UPDATE_AMOUNT';
export const IMPORT_ROLL = 'IMPORT_ROLL';
export const FETCH_DICE_OWNED = 'FETCH_DICE_OWNED';
export const WRITE_DICE_OWNED = 'WRITE_DICE_OWNED';
export const LOADING_DICE_OWNED = 'LOADING_DICE_OWNED';
export const EDIT_DICE_OWNED = 'EDIT_DICE_OWNED';
export const ARMY_BUILDER_DICE_SELECT = 'ARMY_BUILDER_DICE_SELECT'
export const BUILDING_ARMY = 'BUILDING_ARMY';
export const ADD_DICE_TO_ARMY = 'ADD_DICE_TO_ARMY';
export const REMOVE_FIRST_ARMY_DIE = 'REMOVE_FIRST_ARMY_DIE';
export const IMPORT_ARMY = 'IMPORT_ARMY';