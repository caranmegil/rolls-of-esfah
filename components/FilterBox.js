/*

components/FilterBox.js - the view responble for showing die details
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';

import React, {
    useState,
    useEffect,
} from 'react';

import {
    StyleSheet,
    TextInput,
    View,
} from 'react-native';
  
import {
    applyFilter
} from '../actions';

const FilterBox = (props) => {
    function onChangeFilter(value) {
        props.applyFilter(value);
    }

    return (
        <View style={{flex: 1, width: '100%', borderWidth: 1, borderColor: 'black'}}>
            <TextInput placeholder={'Filter'} placeholderTextColor={'green'} onChangeText={onChangeFilter}></TextInput>
        </View>
    )
}

const styles = StyleSheet.create({
});

const mapStateToProps = (state, props) => {
    return {
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        applyFilter: (filter) => dispatch(applyFilter(filter)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FilterBox);