/*

components/ExportButton.js - the button responble exporting the local store for import later
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';
import RNFS from 'react-native-fs';
import {
    Pressable,
    Text,
} from 'react-native';
import React from 'react';
import Share from 'react-native-share';

const ExportArmyButton = (props) => {
    function exportArmy() {
        const filename = `${RNFS.DocumentDirectoryPath}/MyArmy.json`;
        RNFS.writeFile(filename, JSON.stringify(props.armyDice));
        
        Share.open({
            title: 'MyArmy.json',
            urls: [`file://${filename}`],
        }).catch(err => console.log(err));
    }

    return (
        <Pressable onPress={exportArmy}>
            <Text style={{borderRadius: 5, padding: 15, backgroundColor: 'black', color: 'white'}}>EXPORT</Text>
        </Pressable>
    )
}

export default ExportArmyButton;