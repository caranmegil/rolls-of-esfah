/*

components/DiceListItem.js - the view responble for showing die details
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';

import React, {
    useState,
    useEffect,
} from 'react';

import {
    StyleSheet,
    Pressable,
    Text,
    View,
    Image,
    Platform,
    TextPropTypes,
} from 'react-native';
  
import {
    loadingDiceOwned,
    editDiceAmount,
    enableArmyBuilderDiceSelector,
    addDieToArmy,
} from '../actions';

import {
    images
} from '../images';

import { genKey2 } from '../utils';

const DiceListItem = (props) => {
    let amount = 0;
    const key = genKey2(props.item.species, props.item.name, props.item.edition);

    if (props.diceOwned[key] !== undefined) {
        amount = props.diceOwned[key];
    }

    function onDieSelected() {
        if (props.isArmyBuilderDiceSelectorMode) {
            props.addDieToArmy(props.item);
            props.enableArmyBuilderDiceSelector(false);
            props.navigation.navigate('ArmyBuilder');
        } else {
            props.editDiceAmount(props.item.species, props.item.name, props.item.edition, amount);
        }
    }

    let style = {width: 50, height: 50};
    if (props.item.type === 'Monster') {
        style = {width: 36, height: 54};
    }

    const isEven = parseInt(props.index) % 2 == 0
    const containerStyle = (!isEven ? {...styles.container, backgroundColor: 'lightblue', color: 'white'} : {...styles.container})
    const viewStyle = (!isEven ? {...styles.tableCell, backgroundColor: 'lightblue', color: 'white'} : {...styles.tableCell});
    return (
        <Pressable onPress={onDieSelected}>
        <View style={containerStyle}>
                <View style={viewStyle}>
                    {
                        (images[props.item.name] === undefined || images[props.item.name] === '')
                            ? <Text style={{textAlign: 'center', color: 'black'}}>{props.item.name}</Text>
                            : <><Image source={images[props.item.name].req} style={style}/><Text style={{textAlign: 'center', color: 'black'}}>{props.item.name}</Text></>
                    }
                </View>
                <View style={viewStyle}><Text style={{textAlign: 'center', color: 'black'}}>{props.item.type}</Text></View>
                <View style={viewStyle}><Text style={styles.amountCell}>{amount}</Text></View>
        </View>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    tableCell: {
        width: '33%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
    },
    amountCell: {
        textAlign: 'right',
        color: 'black',
    },
});

const mapStateToProps = (state, props) => {
    const {
        diceOwned,
        isArmyBuilderDiceSelectorMode,
    } = state;

    return {
        diceOwned,
        isArmyBuilderDiceSelectorMode,
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        editDiceAmount: (species, name, edition, amount) => dispatch(editDiceAmount(species, name, edition, amount, true)),
        loadingDiceOwned: () => dispatch(loadingDiceOwned()),
        addDieToArmy: (dieToAdd) => dispatch(addDieToArmy(dieToAdd)),
        enableArmyBuilderDiceSelector: (isEnabled) => dispatch(enableArmyBuilderDiceSelector(isEnabled)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DiceListItem);