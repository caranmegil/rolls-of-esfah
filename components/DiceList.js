/*

components/DiceList.js - the list responble for showing the dice
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import React, {
  useEffect,
} from 'react';
import {
  FlatList,
  View,
  Text,
  StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import {v4 as uuidv4} from 'uuid';
import { genKey } from '../utils';

import DiceListItem from './DiceListItem';
  
const DiceList = (props) => {
   const flatListRef = React.createRef()
    let dice = [...props.dice];
 
    dice.sort((a,b) => {
        const keyA = genKey(a);
        const keyB = genKey(b);

        return keyA.localeCompare(keyB)
    });

    const diceLen = dice.length;

    useEffect( () => {
      if (flatListRef.current != null) {
          flatListRef.current.scrollToOffset({
          offset: 0,
          animated: true,
        })
      }
    }, [flatListRef])

    function renderDie(die) {
      return (
        <DiceListItem {...die} navigation={props.navigation}/>
      )
    }

    function keyExtractor(die) {
      return genKey(die);
    }

    return (
      <View style={{flexDirection: 'column', justifyContent: 'space-between'}}>
        <View style={styles.container}>
            <Text style={styles.tableCell}>Name</Text>
            <Text style={styles.tableCell}>Type</Text>
            <Text style={styles.tableCell}>Owned</Text>
          </View>
        <FlatList
          ref={flatListRef}
          style={{borderRadius: 5, height: '65%', alignSelf: 'stretch', flexGrow: 1}}
          data={props.dice}
          renderItem={renderDie}
          keyExtractor={keyExtractor}>
        </FlatList>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
      borderBottomWidth: 1,
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignSelf: 'stretch',

  },
  tableCell: {
      width: '33%',
      alignItems: 'flex-start',
      justifyContent: 'center',
      textAlign: 'center',
      padding: 10,
      fontWeight: 'bold',
      color: 'black',
  },
});

const mapStateToProps = (state, props) => {
    const {
      dice,
    } = state;
    return {
        dice,
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DiceList);
  