/*

components/DieAmountModal.js - the view responble for showing die details
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import React, {
    useEffect,
    useState,
} from 'react';
import {
    connect,
} from 'react-redux';
import {
    upsertDiceOwned,
    loadingDiceOwned,
    editDiceAmount,
} from '../actions';
import {
    View,
    Modal,
    StyleSheet,
    Text,
    Pressable,
} from 'react-native';
import InputSpinner from "react-native-input-spinner";

const DieAmountModal = (props) => {
    const [ownedAmount, setOwnedAmount] = useState( props.currentAmount || 0);

    useEffect( () => {
        setOwnedAmount(props.currentAmount)
    }, [props.currentAmount])

    function onSubmitEditing() {
        if (ownedAmount === '') return;
        const newAmount = parseInt(ownedAmount);
        if (isNaN(newAmount)) return;
        
        props.upsertDiceOwned(props.currentSpecies, props.currentName, props.currentEdition, newAmount);
        setOwnedAmount(newAmount.toString());
        props.editDiceAmount(false);
    }

    function onUpdateAmount(value) {
        setOwnedAmount(value);
    }
    
    function onRequestClose() {
        props.editDiceAmount(false);
    }

    return (
        <View style={styles.centeredView}>
            <Modal
                    animationType="slide"
                    transparent={true}
                    visible={props.isEditingDice}
                    onRequestClose={onRequestClose}
                >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text>{`Please enter an amount for ${props.currentName} of the species ${props.currentSpecies}`}</Text>
                        <InputSpinner skin="square" style={{margin: 15}} min={0} value={ownedAmount} onChange={onUpdateAmount}></InputSpinner>
                        
                        <Pressable onPress={onSubmitEditing}>
                            <Text style={{borderRadius: 5, padding: 15, backgroundColor: 'black', color: 'white'}}>APPLY</Text>
                        </Pressable>
                    </View>
                </View>
            </Modal>
        </View>
    );
}

const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22
    },
    modalView: {
      margin: 20,
      backgroundColor: "white",
      padding: 35,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    button: {
      borderRadius: 5,
      padding: 10,
      elevation: 2
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "#2196F3",
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    }
});
  
const mapStateToProps = (state, props) => {
    const {
        diceEditingState,
    } = state;
    const {
        currentSpecies,
        currentName,
        currentEdition,
        currentAmount,
        isEditingDice,
    } = diceEditingState;
    return {
        currentSpecies,
        currentName,
        currentEdition,
        currentAmount,
        isEditingDice,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        loadingDiceOwned: () => dispatch(loadingDiceOwned()),
        editDiceAmount: (isEditingDice) => dispatch(editDiceAmount('','','',0,isEditingDice)),
        upsertDiceOwned: (species, name, edition, amount) => dispatch(upsertDiceOwned(species, name, edition, amount))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DieAmountModal)