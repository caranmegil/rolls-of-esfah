/*

components/ExportButton.js - the button responble exporting the local store for import later
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';
import RNFS from 'react-native-fs';
import {
    Pressable,
    Text,
} from 'react-native';
import React from 'react';
import Share from 'react-native-share';
import {
    loadingDiceOwned,
} from '../actions';

const ExportButton = (props) => {
    function exportDice() {
        props.loadingDiceOwned(true);
        const filename = `${RNFS.DocumentDirectoryPath}/MyDDCollection.json`;
        RNFS.writeFile(filename, JSON.stringify(props.diceOwned));
        
        Share.open({
            title: 'MyDDCollection.json',
            urls: [`file://${filename}`],
        }).then( () => props.loadingDiceOwned(false) ).catch(err => props.loadingDiceOwned(false));
    }

    return (
        <Pressable onPress={exportDice}>
            <Text style={{borderRadius: 5, padding: 15, backgroundColor: 'black', color: 'white'}}>EXPORT</Text>
        </Pressable>
    )
}

const mapStateToProps = (state, props) => {
    const {
        diceOwned,
    } = state;
    return {
        diceOwned,
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        loadingDiceOwned: isLoading => dispatch(loadingDiceOwned(isLoading)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExportButton);
  