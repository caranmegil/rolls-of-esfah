/*

components/ArmyBuilder.js - the base army builder
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import React, {
    useEffect,
    useState,
} from 'react';
import {
    FlatList,
    View,
    Text,
    Pressable,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import SmoothPicker from "react-native-smooth-picker";
import ArmyBuilderDieItem from './ArmyBuilderDieItem';
import { connect } from 'react-redux';
import {
    enableArmyBuilderDiceSelector,
    addDieToArmy,
    importArmy,
} from '../actions';
import { genKey } from '../utils';
import ImportArmyButton from './ImportArmyButton';
import ExportArmyButton from './ExportArmyButton';

const ArmyBuilder = (props) => {
    const sections = ['Home', 'Campaign', 'Horde', 'Summoning'];
    let armyDiceTemplate = {};
    sections.forEach( section => armyDiceTemplate[section] = [])
    const [currentSection, setCurrentSection] = useState('Home');
    const [sectionPicker, enableSectionPicker] = useState(false);
    const [armyDice, setArmyDice] = useState(armyDiceTemplate);
    
    useEffect(() => {
        const unsub = props.navigation.addListener('focus', () => {
            if (props.dieToAdd != null) {

                let tempArmyDice = {...armyDice}
                tempArmyDice[currentSection].push({
                    'name': props.dieToAdd.name,
                    'type': props.dieToAdd.type,
                    'species': props.dieToAdd.species,
                    'points': determinePoints(props.dieToAdd),
                });

                setArmyDice(tempArmyDice);
                props.addDieToArmy(null);
            }
        });

        return unsub;
    }, [props.navigation, props.dieToAdd, props.importedArmy]);

    useEffect(() => {
        console.log(props.importedArmy)
        if (props.importedArmy != null) {
            setArmyDice(props.importedArmy);
            props.importArmy(null);
        }
    }, [props.importedArmy]);

    function determinePoints(die) {
        switch(die.rarity) {
            case 'Small':
                return 1;
            case 'Medium':
                return 2;
            case 'Large':
                return 3;
            case 'Champion':
            case 'Relic':
            case 'Royalty':
                return 4;
            default:
                return 0;
        }
    }

    function addDie() {
        props.enableArmyBuilderDiceSelector(true);
        props.navigation.navigate('DiePicker');
    }

    function openSection() {
        enableSectionPicker(true);
    }

    function onDieRemoved(die) {
        let purgedArmyDice = armyDice[currentSection];

        for (let dieI=0; dieI < purgedArmyDice.length; dieI++) {
            if (genKey(die) === genKey(purgedArmyDice[dieI])) {
                purgedArmyDice.splice(dieI, 1);
                break;
            }
        }

        let tempArmyDice = {...armyDice};
        tempArmyDice[currentSection] = purgedArmyDice;
        setArmyDice(tempArmyDice);
    }

    function pickSection(item) {
        setCurrentSection(item);
        enableSectionPicker(false);
    }

    function renderDie(die) {
        return (
            <Pressable onPress={() => onDieRemoved(die.item)}>
                <ArmyBuilderDieItem {...die}/>
            </Pressable>
        )
    }

    function keyExtractor(die) {
        return genKey(die);
    }

    return (
        (sectionPicker) ?
            <SmoothPicker
                scrollAnimation
                data={sections}
                selectOnPress={true}
                renderItem={({item, index}) => (
                <TouchableOpacity style={{width: '100%'}} onPress={() => pickSection(item)}>
                    <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 18, borderRadius: 5, padding: 15}}>{item} ({armyDice[item].reduce((value, die) => value + die.points, 0)})</Text>
                </TouchableOpacity>
                )}
            />
        :
        
        <View style={{flexDirection: 'column', justifyContent: 'space-between'}}>
            <View style={styles.horizontal}>
                <Pressable onPress={addDie}>
                    <Text style={{textAlign: 'center', borderRadius: 5, padding: 15, backgroundColor: 'black', color: 'white'}}>ADD</Text>
                </Pressable>
                <ImportArmyButton />
                <ExportArmyButton armyDice={armyDice} />
            </View>
            <Pressable onPress={openSection}>
                <Text style={{textAlign: 'center', borderRadius: 5, padding: 15, backgroundColor: 'black', color: 'white'}}>{currentSection} ({armyDice[currentSection].reduce((value, die) => value + die.points, 0)})</Text>
            </Pressable>
            <View style={styles.container}>
                <Text style={styles.tableCell}>Name</Text>
                <Text style={styles.tableCell}>Type</Text>
                <Text style={styles.tableCell}>Points</Text>
            </View>
            <FlatList
                style={{borderRadius: 5, height: '65%', alignSelf: 'stretch', flexGrow: 1}}
                data={armyDice[currentSection]}
                renderItem={renderDie}
                keyExtractor={keyExtractor}>
            </FlatList>
      </View>
    );
}

const styles = StyleSheet.create({
    container: {
        borderBottomWidth: 1,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignSelf: 'stretch',
  
    },
    tableCell: {
        width: '33%',
        alignItems: 'flex-start',
        justifyContent: 'center',
        textAlign: 'center',
        padding: 10,
        fontWeight: 'bold',
        color: 'black',
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    },    
});
  
const mapStateToProps = (state, props) => {
    const {
        dieToAdd,
        importedArmy,
    } = state;
    return {
        dieToAdd,
        importedArmy,
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        importArmy: (army) => dispatch(importArmy(army)),
        addDieToArmy: (die) => dispatch(addDieToArmy(die)),
        enableArmyBuilderDiceSelector: (modeOn) => dispatch(enableArmyBuilderDiceSelector(modeOn)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ArmyBuilder);