/*

components/ArmyBuilderDieItem.js - the view responble for showing army builder item
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';

import React from 'react';

import {
    StyleSheet,
    Pressable,
    Text,
    View,
    Image,
} from 'react-native';
  
import {
} from '../actions';

import {
    images
} from '../images';

import { genKey2 } from '../utils';

const ArmyBuilderDieItem = (props) => {
    const key = genKey2(props.item.species, props.item.name, props.item.edition);

    let style = {width: 50, height: 50};
    if (props.item.type === 'Monster') {
        style = {width: 36, height: 54};
    }

    const isEven = parseInt(props.index) % 2 == 0
    const containerStyle = (!isEven ? {...styles.container, backgroundColor: 'lightblue', color: 'white'} : {...styles.container})
    const viewStyle = (!isEven ? {...styles.tableCell, backgroundColor: 'lightblue', color: 'white'} : {...styles.tableCell});
    return (
        <View style={containerStyle}>
            <View style={viewStyle}>
                {
                    (images[props.item.name] === undefined || images[props.item.name] === '')
                        ? <Text style={{textAlign: 'center', color: 'black'}}>{props.item.name}</Text>
                        : <><Image source={images[props.item.name].req} style={style}/><Text style={{textAlign: 'center', color: 'black'}}>{props.item.name}</Text></>
                }
            </View>
            <View style={viewStyle}><Text style={{textAlign: 'center', color: 'black'}}>{props.item.type}</Text></View>
            <View style={viewStyle}><Text style={{textAlign: 'center', color: 'black'}}>{props.item.points}</Text></View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    tableCell: {
        width: '33%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
    },
    amountCell: {
        textAlign: 'right',
        color: 'black',
    },
});

const mapStateToProps = (state, props) => {
    return {
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ArmyBuilderDieItem);