import {
    SafeAreaView,
    Text,
    View,
    ImageBackground,
    Pressable,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import React, {
    useState,
    useEffect,
} from 'react';
import {
    connect,
} from 'react-redux';
import SmoothPicker from "react-native-smooth-picker";
import DiceList from '../components/DiceList';
import {
    fetchDiceOwned,
    loadingDiceOwned,
    applyFilter,
} from '../actions';
import {
    menu
} from '../menu';
   
const ArmyBuilderDiePicker = (props) => {
    const [isSpeciesPickerOpen, setSpeciesPickerOpen] = useState(false);
    const [isEditionPickerOpen, setEditionPickerOpen] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);
    const backgroundImage = require('../background.jpeg')
    useEffect(() => {
      if( !isLoaded) {
        props.loadingDiceOwned();
        props.dispatch(fetchDiceOwned());
        
        props.applyFilter('Amazon', '-');
        setIsLoaded(true);
      }
    }, [isLoaded])
  
    const [selectedEdition, setSelectedEdition] = useState(props.filters.editionFilter);
  
    useEffect( () => {
        setSelectedEdition(props.filters.editionFilter);
    }, [props.filters, isEditionPickerOpen])
  
    const [selectedSpecies, setSelectedSpecies] = useState(props.filters.speciesFilter);
  
    useEffect( () => {
        setSelectedSpecies(props.filters.speciesFilter);
    }, [props.filters.speciesFilter, isSpeciesPickerOpen]);
  
    function onSpeciesApply(item) {
      setSelectedSpecies(item);
      props.applyFilter(item, '-');
      setSpeciesPickerOpen(false);
      editions = menu[item];
    }
  
    let species = Object.keys(menu).sort();
    let editions = menu[props.filters.speciesFilter];
  
    function onEditionApply(item) {
      setSelectedEdition(item);
      props.applyFilter(props.filters.speciesFilter, item);
      setEditionPickerOpen(false);
    }
  
    function openSpeciesSelector() {
      setSpeciesPickerOpen(true);
    }
  
    function openEditionSelector() {
      setEditionPickerOpen(true);
    }
  
    return (
      <SafeAreaView>
      {
          isSpeciesPickerOpen ?
            <View style={styles.activityIndicatorContainerSuper}>
              <ImageBackground source={backgroundImage} style={styles.image}>
                <View style={[styles.activityIndicatorContainer, styles.horizontal]}>
                  <View style={{width: '100%',}}>
                    <SmoothPicker
                        scrollAnimation
                        data={species}
                        initialScrollIndex={21}
                        selectOnPress={true}
                        renderItem={({item, index}) => (
                          <TouchableOpacity style={{width: '100%'}} onPress={() => onSpeciesApply(item)}>
                            <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 18, borderRadius: 5, padding: 15}}>{item}</Text>
                          </TouchableOpacity>
                        )}
                    />
                  </View>
                </View>
              </ImageBackground>
            </View>
          :
          isEditionPickerOpen ?
            <View style={styles.activityIndicatorContainerSuper}>
              <ImageBackground source={backgroundImage} style={styles.image}>
                <View style={[styles.activityIndicatorContainer, styles.horizontal]}>
                  <View style={{width: '100%',}}>
                    <SmoothPicker
                        initialScrollToIndex={22}
                        scrollAnimation
                        data={editions}
                        selectOnPress={true}
                        renderItem={({item, index}) => (
                          <TouchableOpacity style={{width: '100%'}} onPress={() => onEditionApply(item)}>
                            <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 18, borderRadius: 5, padding: 15}}>{item}</Text>
                          </TouchableOpacity>
                        )}
                      />
                    </View>
                  </View>
                </ImageBackground>
            </View>
        :
      <View style={styles.containerContainer}>
        <ImageBackground source={backgroundImage} style={styles.image}>
          <View style={styles.container}>
            <View style={{padding: 5}}>
              <Pressable onPress={openSpeciesSelector}>
                <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 18, borderRadius: 5, padding: 15, backgroundColor: 'black', color: 'white'}}>{props.filters.speciesFilter}</Text>
              </Pressable>
            </View>
            <View style={{padding: 5}}>
              <Pressable onPress={openEditionSelector}>
                <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 18, borderRadius: 5, padding: 15, backgroundColor: 'black', color: 'white'}}>{props.filters.editionFilter}</Text>
              </Pressable>
            </View>
            <DiceList navigation={props.navigation}/>
          </View>
        </ImageBackground>
      </View>
      }
      </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    activityIndicatorContainer: {
      flex: 1,
    },
    activityIndicatorContainerSuper: {
      alignSelf: 'center', justifyContent: 'center', alignItems: 'stretch',
    },
    containerContainer: {
      alignSelf: 'flex-start',
    },
    image: {
      flex: 1,
      width: '100%',
      height: '100%',
      top: 0,
      left: 0,
      justifyContent: 'center',
      resizeMode: 'cover',
    },
    image2: {
      flex: 1,
      width: '100%',
      height: '100%',
      top: 0,
      left: 0,
      justifyContent: 'center',
    },
    horizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      padding: 10
    },
    container: {
      justifyContent: 'space-around',
      paddingLeft: 16,
      paddingRight: 16,
    },
    sectionContainer: {
      marginTop: 32,
      paddingHorizontal: 24,
    },
    sectionTitle: {
      fontSize: 24,
      fontWeight: '600',
    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      fontWeight: '400',
    },
    highlight: {
      fontWeight: '700',
    },
    toolbar: {
      backgroundColor: 'black',
    },
    toolbarText: {
      fontSize: 24,
      color: 'white',
      padding: 5,
    },
});

const mapStateToProps = (state, props) => {
    const {
      isLoading,
      filters,
      isArmyBuilderDiceSelectorMode,
      isArmyBuilderActive,
    } = state;
    return {
        isLoading,
        filters,
        isArmyBuilderDiceSelectorMode,
        isArmyBuilderActive,
    };
};
  
const mapDispatchToProps = (dispatch, props) => {
    return {
      enableBuildingArmy: (isBuildingArmy) => dispatch(enableBuildingArmy(isBuildingArmy)),
      applyFilter: (speciesFilter, editionFilter) => dispatch(applyFilter(speciesFilter, editionFilter)),
      loadingDiceOwned: () => dispatch(loadingDiceOwned()),
      dispatch,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ArmyBuilderDiePicker);