# Rolls of Esfah

Rolls of Esfah is a collection management system for the dice game Dragon Dice (TM) from [SFR, Inc.](https://www.sfr-inc.com/).  I give this to the community for free as in freedom and as in beer because in the past individuals have set out on the lofty task of creating similar managers only to have to make their exit from the community (No harm, no foul.  Their reasons are their own.)  This is so that when I make my inevitable exit, a future developer can use it to continue where I left off.

# Features

The features include the following:

* Export of your collection to a specific file format.
* Import of your collection from that specific file foromat.

# Deficiencies

There are some deficiencies that I will work around in future releases:

* Large collection entries are tedious.  I am working on adding the ability to add using the camera on your phone.
* No auto-save.  This seems important now, to me.
* No searching based on name for those familiar with the ID icon already.

# What can I do with the export?

Right now, there are two kinds of things.  You can share the file with others who can read the file themselves in their favorite text editor or import it into their device.  This is inconvenient, so I offer up [My DD Inventory](https://codeberg.org/caranmegil/myddinventory).  This is something that can be added to a site and you just replace the file in data and it will display your collection on the web.  The second kind of thing is the most important: backups.  Right now, I do not have a mechanism to store off-site.  It's coming, but just not ready for prime time yet.